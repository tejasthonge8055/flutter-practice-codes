import 'package:flutter/material.dart';

class Screen6 extends StatelessWidget {
  const Screen6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            color: Color.fromARGB(255, 255, 255, 35),
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Color.fromARGB(255, 201, 123, 226),
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Color.fromARGB(255, 185, 243, 91),
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Color.fromARGB(255, 180, 207, 219),
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Colors.yellow,
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Colors.indigo,
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Colors.blue,
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Color.fromARGB(255, 255, 132, 0),
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Colors.lightBlue,
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Colors.pink,
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
          Container(
            color: Colors.red,
            width:double.infinity,
            height: 90,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
          ),
        ],
      ),
    );
  }
}
